(use-modules (gnu packages ssh)
             (gnu services)
             (gnu services ssh))

(service openssh-service-type
	 (openssh-configuration
	  (openssh openssh-sans-x)
          (permit-root-login #true)
	  (password-authentication? #false)))
