(use-modules
 (gnu services cups))

(service cups-service-type
         (cups-configuration
          (web-interface? #true)))
