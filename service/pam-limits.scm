(use-modules (gnu services)
             (gnu services base)
             (gnu system pam))

(service pam-limits-service-type
         (list
          ;; for gocryptfs
          (pam-limits-entry "*" 'both 'nofile 100000)))
