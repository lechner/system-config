(use-modules (gnu services)
             (gnu services base))

(service guix-publish-service-type
	 (guix-publish-configuration
	  (host "0.0.0.0")
          (port 4849) ; spells G-U-I-X
	  (advertise? #t)))
