(use-modules (gnu services kerberos))

(service krb5-association-service-type
         (krb5-association-configuration
          (default-realm "US-CORE.COM")
          (allow-weak-crypto? #f)
          (realms (list
                   (krb5-realm
                    (name "US-CORE.COM")
                    (admin-server "wallace-server.us-core.com")
                    (kdc "wallace-server.us-core.com"))))))
