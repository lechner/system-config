(use-modules
 (gnu services base))

(udev-rules-service 'net-name-mac
                    (udev-rule
                     "79-net-name-mac.rules"
                     "
SUBSYSTEM==\"net\", ACTION==\"add\", KERNEL!=\"lo*\", NAME=\"$env{ID_NET_NAME_MAC}\"
"))
