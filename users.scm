(list

 (user-account
  (name "lechner")
  (uid 1000)
  (comment "Felix Lechner")
  (group "lechner")
  (supplementary-groups
   (list "audio"
         "cdrom"
         "kvm"
         "netdev"
         "dialout"
         "users"
         "video"
         "wheel")))

 (user-account
  (name "wu")
  (uid 1001)
  (comment "Kate Wu")
  (group "wu")
  (supplementary-groups
   (list "audio"
         "cdrom"
         "netdev"
         "users"
         "video")))

 (user-account
  (name "zhang")
  (uid 1002)
  (comment "Ning Zhang")
  (group "zhang")
  (supplementary-groups
   (list "audio"
         "cdrom"
         "netdev"
         "users"
         "video")))

 (user-account
  (name "lee")
  (uid 1003)
  (comment "Lee Lechner")
  (group "lee")
  (supplementary-groups
   (list "audio"
         "cdrom"
         "netdev"
         "users"
         "video")))

  (user-account
  (name "stella")
  (uid 1004)
  (comment "Stella Romi Lechner")
  (group "stella")
  (supplementary-groups
   (list "audio"
         "cdrom"
         "netdev"
         "users"
         "video"))))
