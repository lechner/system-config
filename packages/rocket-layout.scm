(use-modules
 (gnu packages xorg)
 (guix build-system copy)
 (guix git-download)
 ((guix licenses) #:prefix license:))

(define-public rocket-layout
  (let ((commit "4abbc2ad9370b4eacaf3d1ef413e22209765018a")
        (version "0.0")
        (revision "1"))
    (package
      (name "rocket-layout")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://codeberg.org/lechner/rocket-layout.git")
                      (commit commit)))
                (sha256
                 (base32
                  "02rw79vk1jk6zrd27ih8nzh470j3pzhry6ih440bk3gha1753fnz"))
                (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments (quote (#:install-plan
                         (quote (("xkb/symbols/rocket" "share/xkb/"))))))
      (synopsis "The Rocket Keyboard Layout - Supercharge Your Typing")
      (description
       "Type Control combinations with ease. Turn your Space Bar into a Control key.
Take a moment: Which key should be in the center of your keyboard? Inspired by the
brilliant Halmak, Workman, and Colemak layouts. Also has some similiarities to
Spacemacs.")
      (home-page "https://codeberg.org/lechner/rocket-layout")
      (license license:expat))))

(define-public my-xkeyboard-config
  (let ((xkeyboard-config xkeyboard-config))
    (package (inherit xkeyboard-config)
             ;; (name "my-xkeyboard-config")
             (inputs
              (modify-inputs (package-inputs xkeyboard-config)
                (append rocket-layout)))
             (native-inputs
              (modify-inputs (package-native-inputs xkeyboard-config)
                (append xmlstarlet)))
             (arguments
              (substitute-keyword-arguments (package-arguments xkeyboard-config)
                ((#:phases phases '%standard-phases)
	         `(modify-phases ,phases
		    (add-after 'unpack 'add-layouts
		      (lambda* (#:key inputs #:allow-other-keys)
                        (define (add-layout id country label symbols-file)
                          ;; append custom symbols for country
                          (let* ((country-file (string-append "symbols/" country))
                                 (country-port (open-file country-file "a")))
                            (display "\n" country-port)
                            (call-with-input-file symbols-file
                              (lambda (input-port)
                                (dump-port input-port country-port)))
                            (close-port country-port))
                          ;; add to XML registry
                          ;; based on https://stackoverflow.com/a/9172796
                          (system* "xmlstarlet" "edit" "--inplace"
                                   "--subnode" (string-append
                                                "/xkbConfigRegistry/layoutList/layout[configItem[name='"
                                                country
                                                "']]/variantList")
                                   "--type" "elem"
                                   "--name" "newVariant"
                                   "--subnode" "//newVariant"
                                   "--type" "elem"
                                   "--name" "newConfigItem"
                                   "--subnode" "//newConfigItem"
                                   "--type" "elem"
                                   "--name" "name"
                                   "--value" id
                                   "--subnode" "//newConfigItem"
                                   "--type" "elem"
                                   "--name" "description"
                                   "--value" label
                                   "--rename" "//newVariant"
                                   "--value" "variant"
                                   "--rename" "//newConfigItem"
                                   "--value" "configItem"
                                   "rules/base.xml"))
                        (add-layout "rocket" "us" "English (Rocket)"
                                    (search-input-file inputs "/share/xkb/rocket")))))))))))

;; (set! xkeyboard-config my-xkeyboard-config)
