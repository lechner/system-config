(use-modules (gnu services)
             (gnu services base))

(list
 (service greetd-service-type
          (greetd-configuration
           (greeter-supplementary-groups
            ;; add 'seat' for seatd
            (list "video" "input"))
           (terminals
            (list (greetd-terminal-configuration
                   (terminal-vt "1")
                   (terminal-switch #t))
                  ;; (default-session-command
                  ;;   (greetd-wlgreet-sway-session
                  ;;    (sway sway-latest)
                  ;;    (wlgreet-session
                  ;;     (greetd-wlgreet-session
                  ;;      (command (file-append sway-latest "/bin/sway"))))
                  ;;    (sway-configuration
                  ;;     (config-file "sway-greetd.conf")))))
                  (greetd-terminal-configuration
                   (terminal-vt "2")
                   (terminal-switch #t))
                  (greetd-terminal-configuration
                   (terminal-vt "3")
                   (terminal-switch #t))
                  (greetd-terminal-configuration
                   (terminal-vt "4")
                   (terminal-switch #t))
                  (greetd-terminal-configuration
                   (terminal-vt "5")
                   (terminal-switch #t))
                  (greetd-terminal-configuration
                   (terminal-vt "6")
                   (terminal-switch #t))))))
 (service mingetty-service-type
          (mingetty-configuration (tty "tty8"))))
