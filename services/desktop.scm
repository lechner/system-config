(use-modules (gnu packages libusb)
             (gnu packages suckless)
             (gnu packages xdisorg)
             (gnu services)
             (gnu services dbus)
             (gnu services desktop)
             (gnu services networking)
             (gnu services pm)
             (gnu services sound)
             (gnu services xorg)
             (guix gexp))

(list
 (service alsa-service-type)
 (service network-manager-service-type)
 (service elogind-service-type)
 (service wpa-supplicant-service-type)
 (service usb-modeswitch-service-type)
 (service udisks-service-type)
 (service cups-pk-helper-service-type)
 (service colord-service-type)
 (service dbus-root-service-type)
 (service sane-service-type)
 fontconfig-file-system-service
 (service x11-socket-directory-service-type)
 (simple-service 'mtp udev-service-type
                 (list libmtp))
 (service thermald-service-type)
 #;
 (service pipewire-service-type
 (pipewire-configuration
 (debug? #t)))
 (service pulseaudio-service-type))
