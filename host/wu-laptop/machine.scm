(list (machine
       (operating-system (load "operating-system.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "wu-laptop.local")
                       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJdt/7+XKng/yWvinua8TSFVEu+rhCgKEk02+WpQa26B root@wu-laptop")
                       (system "x86_64-linux")
                       (allow-downgrades? #t)
                       (identity "/home/lechner/.ssh/id_ed25519")))))
