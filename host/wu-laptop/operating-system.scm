(use-modules (gnu)
             (gnu packages)
             (gnu packages certs)
             (gnu packages disk)
             (gnu packages file-systems)
             (gnu packages gnome)
             (gnu packages kerberos)
             (gnu packages linux)
             (gnu packages shells)
             (gnu packages terminals)
             (gnu packages wm)
             (gnu services desktop)
             (gnu services mail)
             (gnu services xorg)
             (gnu system nss)
             (nongnu packages linux)
             (nongnu system linux-initrd))

(operating-system
  (host-name "wu-laptop")
  (timezone "America/Los_Angeles")
  (locale "en_US.utf8")

  (kernel linux)
  (kernel-arguments (cons "consoleblank=60"
                          %default-kernel-arguments))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  (keyboard-layout (keyboard-layout "us" "altgr-intl"))

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets (list "/efi"))
               (keyboard-layout keyboard-layout)))

  (mapped-devices
   (list (mapped-device
          (source "satellitevg")
          (targets (list "satellitevg-guix"))
          (type lvm-device-mapping))))

  (file-systems (append
                 (list (file-system
                         (device (file-system-label "GUIX"))
                         (mount-point "/")
                         (type "ext4")
                         (dependencies mapped-devices))
                       (file-system
                         (device (file-system-label "EFI_JSPCUIT"))
                         (mount-point "/efi")
                         (type "vfat")
		         (options "dmask=027,fmask=137")))

                 %base-file-systems))

  (swap-devices
   (list
    (swap-space
     (target (file-system-label "swap"))
     (dependencies mapped-devices))))

  (users (append
          (load "../../users.scm")
          %base-user-accounts))

  (groups (append
           (load "../../groups.scm")
           %base-groups))

  (packages (append (list

                     efibootmgr
                     gocryptfs
                     gptfdisk
                     heimdal
                     lvm2
                     mdadm
                     i915-firmware
		     ;; for user mounts
		     gvfs)

		    %base-packages))

  (services
   (append (list

	    (service gnome-desktop-service-type)

	    (set-xorg-configuration
             (xorg-configuration
              (keyboard-layout keyboard-layout)))

            (service opensmtpd-service-type
                     (opensmtpd-configuration
                      (shepherd-requirement '(networking))
                      (config-file
                       (plain-file "opensmtpd-configuration"
                                   "
table secrets file:/etc/mail/relay.secrets
listen on localhost
action \"relay\" relay host smtp+tls://wu-laptop@submission.us-core.com:587 auth <secrets>
match from local for any action \"relay\"
"))))

            ;; for gocryptfs
            (load "../../service/pam-limits.scm")
            (load "../../service/openssh.scm")
            (load "../../service/krb5-association.scm")
            (load "../../service/cups.scm")
            (load "../../service/udev-rules-net-name-mac.scm")
	    (load "../../service/guix-publish.scm"))

	   (modify-services
	       %desktop-services

	     (guix-service-type
	      config => (guix-configuration
		         (inherit config)
		         (substitute-urls
			  (append (list "https://substitutes.nonguix.org")
				  %default-substitute-urls))
                         (discover? #t)
		         (authorized-keys
			  (append (list
				   (plain-file "lechner-desktop.pub"
					       "(public-key (ecc (curve Ed25519) (q #219656830F8DB4B7D45358F6AB9DE5FCD2877195D731A3B283A575983443C3DC#)))")
				   (plain-file "non-guix.pub"
					       "(public-key (ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))

				  %default-authorized-guix-keys)))))

	   ))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
