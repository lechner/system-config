(use-modules
 (gnu packages admin)
 (gnu packages certs)
 (gnu packages cups)
 (gnu packages disk)
 (gnu packages file-systems)
 (gnu packages golang)
 (gnu packages kerberos)
 (gnu packages linux)
 (gnu packages mail)
 (gnu packages nfs)
 (gnu packages xorg)
 (gnu services avahi)
 (gnu services configuration)
 (gnu services linux)
 (gnu services mail)
 (gnu services networking)
 (gnu services nfs)
 (gnu services pam)
 (gnu services shepherd)
 (gnu system nss)
 (gnu)
 (guix packages)
 (nongnu packages linux)
 (nongnu system linux-initrd))

(load "../../packages/rocket-layout.scm")

(define welcome-pamda-file
  (scheme-file
   "welcome-pamda-file"
   #~(begin
       (use-modules ((pam) #:prefix guile-pam:))

       (lambda (action handle flags options)
         (let* ((text (case action
                        ;; authentication management
                        ((pam_sm_authenticate)
                         "In a working module, we would now identify you.")
                        ((pam_sm_setcred)
                         "In a working module, we would now help you manage additional credentials.")
                        ;; account management
                        ((pam_sm_acct_mgmt)
                         "In a working module, we would now confirm your access rights.")
                        ;; password management
                        ((pam_sm_chauthtok)
                         "In a working module, we would now alter your password.")
                        ;; session management
                        ((pam_sm_open_session)
                         "In a working module, we would now open a session for you.")
                        ((pam_sm_close_session)
                         "In a working module, we would now close your session.")
                        (else
                         (error "Unknown action" action)))))
           (guile-pam:message handle 'PAM_TEXT_INFO
                              text))
         'PAM_IGNORE))))

(define motd
  (plain-file
   "motd"
   "This is your message of the day."))

(define my-pamda-file
  (scheme-file
   "my-pamda-file"
   #~(begin
       (use-modules ((pam) #:prefix guile-pam:)
                    (pam legacy module)
                    (pam stack))
       (lambda (action handle flags options)
         ;; for gocryptfs
         (if (eq? 'pam_sm_open_session action)
             (setrlimit 'nofile 100000 100000))
         (stack action handle flags
                (list
                 (gate optional (load #$welcome-pamda-file))
                 (let* ((username (guile-pam:get-username handle))
                        (hostname (gethostname))
                        (file (string-append "/acct/"
                                             username
                                             "/away/pam.scm")))
                   (gate required (load #$(file-append guile-pam
                                                       "/share/modules/user-session-with-piped-secret.scm"))
                         #:options (list file)
                         #:only-services '("login"
                                           "greetd"
                                           "su"
                                           "slim"
                                           "gdm-password"
                                           "sddm")))
                 (gate optional
                       (lambda (action handle flags options)
                         (call-shared-object #$(file-append linux-pam "/lib/security/pam_motd.so")
                                             action handle flags
				             #:options
                                             (list (string-append "motd=" #$motd))
                                             #:implements
                                             '(pam_sm_open_session))))))))))

(operating-system
  (host-name "lechner-desktop")
  (timezone "America/Los_Angeles")
  (locale "en_US.utf8")

  (kernel linux)
  (kernel-arguments (cons "consoleblank=60"
                          %default-kernel-arguments))
  (initrd microcode-initrd)
  (initrd-modules (cons* "mpt3sas"
                         "raid1"
                         %base-initrd-modules))
  (firmware (list linux-firmware))

  (keyboard-layout (keyboard-layout "us" "altgr-intl"))
  ;; (keyboard-layout (keyboard-layout "us" "rocket"))

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets (list "/efi"))
               (keyboard-layout keyboard-layout)))

  (mapped-devices (list

                   (mapped-device
                    (source (uuid "40c419b2:692f5394:9e267d27:7f8a2d92" 'mdadm))
                    (target "/dev/md15")
                    (type md-array-device-mapping))

                   (mapped-device
                    (source "optivg")
                    (targets (list "optivg-guix"))
                    (type lvm-device-mapping))))

  (file-systems
   (append (list

            (file-system
              (device (file-system-label "GUIX"))
              (mount-point "/")
              (type "ext4")
              (dependencies mapped-devices))

            (file-system
              (device (file-system-label "EFI_FA68792"))
              (mount-point "/efi")
              (type "vfat"))

            (file-system
              (device "wallace-server.local:/acct")
              (mount-point "/acct")
              (type "nfs")
              (shepherd-requirements '(avahi-daemon)) ;resolve .local
              ;; (flags '(no-atime no-dev no-exec read-only))
              ;; (options "proto=tcp6,timeo=300,nolock")
              (check? #f)
              (mount-may-fail? #t)
              (create-mount-point? #t))

            (file-system
              (device "/dev/mapper/optivg-home.local")
              (mount-point "/lcl")
              (type "ext4")
              (dependencies mapped-devices))

            (file-system
              (device (file-system-label "FSCACHE"))
              (mount-point "/var/cache/fscache")
              (type "ext4")
              (dependencies mapped-devices))
            )

           %base-file-systems))

  (swap-devices
   (list
    (swap-space
      (target (file-system-label "swap"))
      (dependencies mapped-devices))))

  (packages
   (append (list

            cups
            efibootmgr
            gocryptfs
            gptfdisk
            heimdal
            linux-libre-headers
            lvm2
            mdadm
            nfs-utils
            smartmontools)

           %base-packages))

  (users (append
          (load "../../users.scm")
          %base-user-accounts))

  (groups (append
           (load "../../groups.scm")
           %base-groups))

  (services
   (append (list

            (service guile-pam-module-service-type
                     (guile-pam-module-configuration
                      (rules "optional")
                      (module my-pamda-file)
                      (services '("login"
                                  "greetd"
                                  "su"
                                  "slim"
                                  "gdm-password"
                                  "sddm"))))

            (service avahi-service-type) ; for guix-publish
            (service ntp-service-type)

            ;; (service qemu-binfmt-service-type
            ;;          (qemu-binfmt-configuration
            ;;           (platforms (lookup-qemu-platforms "aarch64" "riscv64"))))

            (service cachefilesd-service-type
                     (cachefilesd-configuration
                      (cache-directory "/var/cache/fscache")))

            (load "../../service/openssh.scm")
            (load "../../service/krb5-association.scm")
            (load "../../service/cups.scm")
            (load "../../service/udev-rules-net-name-mac.scm")

            (service opensmtpd-service-type
                     (opensmtpd-configuration
                      (shepherd-requirement '(networking))
                      (config-file
                       (plain-file "opensmtpd-configuration"
                                   "
table secrets file:/etc/mail/secrets

listen on lo port 25
listen on socket

action \"transmit\" relay host \"smtp+tls://lechner-desktop@submission.us-core.com:587\" auth <secrets> mail-from \"@us-core.com\"
match for any action \"transmit\"
"))))


            ;;                   (service rpcbind-service-type)
            ;;                   (service pipefs-service-type
            ;;                          (pipefs-configuration (mount-point "/var/lib/nfs/rpc_pipefs")))
            ;;                   (service gss-service-type)

            (load "../../service/guix-publish.scm"))

           (load "../../services/greeter.scm")
           (load "../../services/desktop.scm")

           (modify-services %base-services

             (delete agetty-service-type)
             (delete mingetty-service-type)

             (guix-service-type
              config =>
              (guix-configuration

               (inherit config)
               (extra-options (list "--gc-keep-outputs=yes"))
               (substitute-urls
                (append (list "https://substitutes.nonguix.org")
                        %default-substitute-urls))
               (discover? #t)
               (authorized-keys
                (append (list
                         (plain-file "wallace-server.pub"
                                     "(public-key (ecc (curve Ed25519) (q #87E0A28A63E228A7B83762561B6A7243CE2C80208B2DF3CD3E7CDE777D51CC2B#)))")
                         (plain-file "genenetwork.pub"
                                     "(public-key (ecc (curve Ed25519) (q #9578AD6CDB23BA51F9C4185D5D5A32A7EEB47ACDD55F1CCB8CEE4E0570FBF961#)))")
                         (plain-file "non-guix.pub"
                                     "(public-key (ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))

                        %default-authorized-guix-keys)))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
