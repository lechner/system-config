(list (machine
       (operating-system (load "operating-system.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "wallace-server.local")
                       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE9DGx9bJ8mqAZ4R5hvY7mcXqfHog9gdCh7+q8b4fTTR root@wallace-server")
                       (system "x86_64-linux")
                       (allow-downgrades? #t)
                       (identity "/home/lechner/.ssh/id_ed25519")))))
