(use-modules (gnu)
             (gnu packages)
             (gnu packages admin)
             (gnu packages backup)
             (gnu packages certs)
             (gnu packages databases)
             (gnu packages disk)
             (gnu packages file-systems)
             (gnu packages guile-xyz)
             (gnu packages irc)
             (gnu packages kerberos)
             (gnu packages linux)
             (gnu packages mail)
             (gnu packages messaging)
             (gnu packages networking)
             (gnu packages nfs)
             (gnu packages package-management)
             (gnu packages rsync)
             (gnu packages scsi)
             (gnu packages tls)
             (gnu services avahi)
             (gnu services certbot)
             (gnu services configuration)
             (gnu services databases)
             (gnu services desktop)
             (gnu services dns)
             (gnu services kerberos heimdal)
             (gnu services mail)
             (gnu services messaging)
             (gnu services networking)
             (gnu services nfs)
             (gnu services samba)
             (gnu services shepherd)
             (gnu services web)
             (gnu system nss)
             (guix git-download)
             (guix modules)
             (guix packages)
             (guix records)
             (ice-9 format)
             (juix deploy debbugs)
             (juix deploy guile-avatar)
             (juix deploy guile-naptcha)
             (juix deploy irc-helper-bot)
             (juix deploy mumi)
             (juix deploy patchwise)
             (nongnu packages linux)
             (nongnu system linux-initrd))

(define-public mumi/local
  (let ((commit "c07c1ecfb4c2a71a1cd7685758458648d2bbb3c0")
        (revision "0"))
    (package
      (inherit mumi)
      (name "mumi")
      (version (git-version (package-version mumi) revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://codeberg.org/lechner/mumi.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0afc9fa37vh7lrwh9914lynw6iw9b6bdcxpik2m49rnrr5ckajnm"))))
      (inputs
       (modify-inputs (package-inputs mumi)
         (prepend guile-avatar/juix
                  guile-naptcha/juix
                  haunt))))))

(define our-policy
  (knot-remote-configuration
   (id "default")))

(define our-slave-servers
  (list

   (knot-remote-configuration
    (id "kjsl_slave")
    (address (list "204.87.183.53" "2607:7c80:54:6::53")))
   (knot-remote-configuration
    (id "puck_slave")
    (address (list "204.42.254.5" "2602:fe55:5::5")))))

(define our-acls
  (list

   (knot-acl-configuration
    (id "kjsl_acl")
    (action (list "transfer"))
    (address (list "204.87.183.53" "2607:7c80:54:6::53")))
   (knot-acl-configuration
    (id "puck_acl")
    (action (list "transfer"))
    (address (list "204.42.254.5" "2602:fe55:5::5")))))

(define-zone-entries name-server-entries
  ("@"  ""  "IN"  "NS"      "ns1.us-core.com.")
  ("@"  ""  "IN"  "NS"      "ns-global.kjsl.com.")
  ("@"  ""  "IN"  "NS"      "puck.nether.net."))

(define-zone-entries mail-entries
  ("@"  ""  "IN"  "MX"      "10 sail-ipv4.us-core.com.")

  ("@"                 ""  "IN"  "TXT"    "\"v=spf1 a:submission.us-core.com mx a include:_spf.google.com -all\"")
  ("_dmarc"            ""  "IN"  "TXT"    "\"v=DMARC1;p=reject;fo=1;ruf=mailto:postmaster@us-core.com\"")
  ("2017._domainkey"   ""  "IN"  "TXT"    "\"v=DKIM1;k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCvjHuz7si+3L2krMqXWmzt5OjXZEfC2tqcYWWrPTywj98ln5fKwUcthAI4gjhMjGsS07+1D6QB2zlp+byiQSND/ubpyG3U5+TEAzagtyuDeJUWTK6xMB2/fPD8xPcvRvU5kBn5VofBIjtNcv7cM2jQiun7WLvKBIemAl5/wRNFzwIDAQAB\"")
  ("2020._domainkey"   ""  "IN"  "TXT"    "\"v=DKIM1;k=ed25519;p=xxRMtxPgHwzB+ELIPC84pjNxo/WFik8x316Qp32SgBY=\"")
  ("_adsp._domainkey"  ""  "IN"  "TXT"    "\"dkim=unknown\"")
  ("_mta-sts"          ""  "IN"  "TXT"    "\"v=STSv1;id=aca9f86d665;\"")
  ("_smtp._tls"        ""  "IN"  "TXT"    "\"v=TLSRPTv1;rua=mailto:postmaster@us-core.com\"")
  ("mta-sts"           ""  "IN"  "CNAME"  "sail-ipv4.us-core.com."))

(define-zone-entries bare-address-entries
  ("@"    ""  "IN"  "A"       "208.82.101.137"))

(define-zone-entries tls-entries
  ("@"    ""  "IN"  "CAA"     "0 issue \"letsencrypt.org\""))

(define-zone-entries us-core.com-entries
  ("www"  ""  "IN"  "CNAME"   "us-core.com.")

  ("@"  ""  "IN"  "TXT"     "\"google-site-verification=oAA_jg_CNx7GXpOrncai1gYa_ojZuMQ7lpCojyUlDaE\"")

  ("kerberos"          ""  "IN"  "CNAME"  "wallace-server.us-core.com.")
  ("kerberos-admin"    ""  "IN"  "CNAME"  "wallace-server.us-core.com.")
  ("_kerberos"         ""  "IN"  "TXT"    "\"US-CORE.COM\"")
  ("_kerberos-adm"     ""  "IN"  "SRV"    "0 0 749 wallace-server.us-core.com.")

  ("_kerberos._udp"    ""  "IN"  "SRV"    "0 0 88 wallace-server.us-core.com.")
  ("_kerberos-master"  ""  "IN"  "SRV"    "0 0 88 wallace-server.us-core.com.")
  ("_kpasswd._udp"     ""  "IN"  "SRV"    "0 0 464 wallace-server.us-core.com.")

  ("submission"        ""  "IN"  "CNAME"  "sail-ipv4")
  ("_submission._tcp"  ""  "IN"  "SRV"    "0 1 587 sail-ipv4")

  ("imap"              ""  "IN"  "CNAME"  "sail-ipv4")
  ("_imaps._tcp"       ""  "IN"  "SRV"    "0 1 995 sail-ipv4")

  ("ldap"              ""  "IN"  "CNAME"  "wallace-server.us-core.com.")
  ("_ldap._tcp"        ""  "IN"  "SRV"    "0 0 389 wallace-server.us-core.com.")

  ("ns1"    ""  "IN"  "A"       "208.82.101.137")

  ("sail-ipv4" ""  "IN"  "A"       "208.82.101.137")

  ("apricot-router" ""  "IN"  "A"       "208.82.101.137")
  ("apricot-router" ""  "IN"  "SSHFP"   "1 1 734bc8674e1fa604d8f207d8a01ff0edeb238208")
  ("apricot-router" ""  "IN"  "SSHFP"   "1 2 e91ebaf72b1f29baac311126be73b5e2905af7f5b5c44c1d6023a94d0167e328")
  ("apricot-router" ""  "IN"  "SSHFP"   "4 1 9473bc392c8eb5639c51fd4fc846254f5ee5ce3b")
  ("apricot-router" ""  "IN"  "SSHFP"   "4 2 46ac51eb402ea40c3f6aa5d6f93372cbb79015eb56e466dff4a3660266e5898d")

  ("lechner-desktop" ""  "IN"  "SSHFP"  "1 1 eb9078ed489dd2b963e63947b424ebcc0e4e2742")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "1 2 f55f327f42682fd23fe44543d9ebe65c6c4b6c2f4aeba10c38b1727989de2744")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "2 1 e783bd8b81f3e8daa04fdd1a2899d39a8391ca22")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "2 2 d7d92c29fd936890f0d2c009654a18fa6c42d763d74d9026d912b93cfa762a10")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "3 1 cac5c3393486fc887c74490e0a0ccda93150e084")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "3 2 a4eea43d92ecfc0fbea7393971310ee130d1e362bf888940d0744dff0ce12cf9")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "4 1 d7362347b096ad03ffba98dbebf21a581ad6d3c8")
  ("lechner-desktop" ""  "IN"  "SSHFP"  "4 2 2fd9f911438db9431485bda2beace5b26997d333151f24bfc9caa43afb53938a")

  ("lechner-laptop" ""  "IN"  "SSHFP"   "1 1 ed70ecf8d72ca4ca8b5d61cee5d423deee8797af")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "1 2 b31e809f27fb43bcfb93edc010c20b5db850693fb54f2a852afe1fc517e6c190")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "2 1 20f0fca8983c26a1be6deed78b64d7c959a31bbe")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "2 2 8a5b52a1dc96acb536652a488f8364c62526475f943799012e7dbdcff72236f6")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "3 1 9f09a20409a684cc5e29c5b2616585b8d98ecb28")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "3 2 368f93facf79cb4cca8fdfcfc71ed2d91437bd62b800f65a55251de81dee94b4")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "4 1 1f766aace7d3d58f212ff97e48c7f641b0382d69")
  ("lechner-laptop" ""  "IN"  "SSHFP"   "4 2 0fb4dc4a2d6c7c366c31a46c74905cbb7fbaa93216cc564a3a151851924d75b4")

  ("lee-desktop" ""  "IN"  "SSHFP"      "1 1 4818c6449c4897358c2403110c506b08e118dfaf")
  ("lee-desktop" ""  "IN"  "SSHFP"      "1 2 29d138b433dd2c822fcb6cf69bc1557092c74c097a1a3332e2b8fb151289b0c5")
  ("lee-desktop" ""  "IN"  "SSHFP"      "2 1 548fb0b43a55bce17b279153f6684e86453609a6")
  ("lee-desktop" ""  "IN"  "SSHFP"      "2 2 b506e1e8ed9cd3f0b5e1a0155e74ec92c4d10297f3c2dc335e8fadb3fd101914")
  ("lee-desktop" ""  "IN"  "SSHFP"      "3 1 df630e3290432a33aacf35d24b674b95b93573e5")
  ("lee-desktop" ""  "IN"  "SSHFP"      "3 2 b84f5cd7b4d74d3ebe4484880a312e6c89e19f72fba7d0beecff482c3f0cfaf3")
  ("lee-desktop" ""  "IN"  "SSHFP"      "4 1 3c2bb9b68b47ad34373668fa3a45460028d15ba5")
  ("lee-desktop" ""  "IN"  "SSHFP"      "4 2 2900dba2a0e58b54fe7a2461fe00c64f93e07489e136b8248e07bd46fd4283b4")

  ("wallace-server" ""  "IN"  "SSHFP"   "1 1 64e2cefe95108ecfe361490c3a07a46af5ee2511")
  ("wallace-server" ""  "IN"  "SSHFP"   "1 2 5f4dbcd62e020dde7379b8d37457349a87456020f5ed962db50150fc09913f7e")
  ("wallace-server" ""  "IN"  "SSHFP"   "2 1 f7fedefeebe3c1eb5de88fab963a4da555d93bf0")
  ("wallace-server" ""  "IN"  "SSHFP"   "2 2 19e1fb66c8d4966a5580e220e5e8864c2242a974ba2cd9f4d872ad331d3f698d")
  ("wallace-server" ""  "IN"  "SSHFP"   "3 1 2ee9f081daa6f74d1cfd2cda5d78b3bdf5f10e54")
  ("wallace-server" ""  "IN"  "SSHFP"   "3 2 4019618f59f901d6c66a5669a476d577ac383df115f935f8f01210312a8017da")
  ("wallace-server" ""  "IN"  "SSHFP"   "4 1 5290ab4a97fe0f490fa366b45f14a99339a0b006")
  ("wallace-server" ""  "IN"  "SSHFP"   "4 2 cff4593b42dda0f6d99a95f21166db2968886468fccc7a02b18f1f8bee3dcdb2")

  ("wu-laptop" ""  "IN"  "SSHFP"        "1 1 16c0297e9dc383cb743055c9ba777bc26070b3bc")
  ("wu-laptop" ""  "IN"  "SSHFP"        "1 2 b000b269775af734ee3d845c2474a8a50831548530df71604370db4c6a0d74e1")
  ("wu-laptop" ""  "IN"  "SSHFP"        "2 1 1b69044a64eb0e06e614ac64f1fbe25fadb0d7ef")
  ("wu-laptop" ""  "IN"  "SSHFP"        "2 2 e202cf371c505324f07cb4334dd983a353346e813cabb40236fb377f5e71132f")
  ("wu-laptop" ""  "IN"  "SSHFP"        "3 1 aa35e243d3c92ab5191acdc577279d0ee0d1b453")
  ("wu-laptop" ""  "IN"  "SSHFP"        "3 2 f7d034284353558c72af12e945e6ae3a6493c00b038b31f52d8749183c2933c5")
  ("wu-laptop" ""  "IN"  "SSHFP"        "4 1 a9cc3563d805ac74e4260a31fbab618dd6789347")
  ("wu-laptop" ""  "IN"  "SSHFP"        "4 2 fcaa9bad64bef9f7c633e9496d9ba2bdbf896b95bf87ab53dd2fefb733fd5f54"))

(define master-zones
  (list

   (knot-zone-configuration
    (domain "28palms.org")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "28palms.org")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "28palms.org."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=8sSUhMaEP58H3hXYlk5ydO4rwGv6WRt0iHdnixqX8LM\"")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "amegrund.com")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "amegrund.com")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "amegrund.com."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=pntnE9uHt43RNDf9lWbr4yltGgW2EuLc-HfxDcDeQg0\"")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "dazzledigs.com")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "dazzledigs.com")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "dazzledigs.com.")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "dazzledigs.net")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "dazzledigs.net")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "dazzledigs.net.")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "dazzledigs.org")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "dazzledigs.org")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "dazzledigs.org.")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "fremontmip.org")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "fremontmip.org")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "fremontmip.org."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=ZB5j__BEF88PAHQsp0KBS9mqmmXFcH3lnD7kUal4D4M\"")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "innovametro.com")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "innovametro.com")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "innovametro.com."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=AXklBHYiHE_s1cLfT5V4XyoI2yxrIJengtcPF-wSH7I\"")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "juix.org")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "juix.org")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "juix.org."))
                      (zone-entry (name "debbugs") (type "CNAME") (data "juix.org."))
                      (zone-entry (name "debbugs-latest") (type "CNAME") (data "juix.org."))
                      (zone-entry (name "find") (type "CNAME") (data "juix.org."))
                      (zone-entry (name "get") (type "CNAME") (data "juix.org."))
                      (zone-entry (name "mumi") (type "CNAME") (data "juix.org.")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "lease-up.com")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "lease-up.com")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "lease-up.com."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=9JYc5pIJcnzf0KG-p7IEfYrdHPmhJvNEiTZV-e3MYgk\""))
                      (zone-entry (name "contacts") (type "CNAME") (data "lease-up.com."))
                      (zone-entry (name "_xmpp-client._tcp")
                                  (type "SRV")
                                  (data "20 0 5222 lease-up.com."))
                      (zone-entry (name "_xmpp-server._tcp")
                                  (type "SRV")
                                  (data "20 0 5269 lease-up.com.")))                                        tls-entries)))))

   (knot-zone-configuration
    (domain "patchwise.org")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "patchwise.org")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "patchwise.org.")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "stop4kids.com")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "stop4kids.com")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "stop4kids.com."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=kYZxzhvH-DBrIw9YMHJJX_hmJYrgLxtnOi4AmxY2QdU\"")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "stop4kids.org")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "stop4kids.org")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "stop4kids.org."))
                      (zone-entry (type "TXT") (data "\"google-site-verification=rCbMFuyYWo9pi_cTxq7XH-2jDNrlpuHsMWYRUwyxS0g\"")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "us-core.com")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "us-core.com")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     tls-entries
                     us-core.com-entries)))))

   (knot-zone-configuration
    (domain "vivrea.co")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "vivrea.co")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "vivrea.co.")))
                     tls-entries)))))

   (knot-zone-configuration
    (domain "vivrea.us")
    (acl (list "kjsl_acl" "puck_acl"))
    (notify (list "kjsl_slave" "puck_slave"))
    (dnssec-policy "default")
    (serial-policy (quote unixtime))
    (zone (zone-file
           (origin "vivrea.us")
           (serial (current-time))
           (ns "ns1.us-core.com.")
           (entries (append
                     name-server-entries
                     mail-entries
                     bare-address-entries
                     (list
                      (zone-entry (name "www") (type "CNAME") (data "vivrea.us.")))
                     tls-entries)))))))

(define local-mail-domains
  (plain-file
   "local-mail-domains"
   "
28palms.org
amegrund.com
dazzledigs.com
dazzledigs.net
dazzledigs.org
fremontmip.org
innovametro.com
juix.org
lease-up.com
patchwise.org
stop4kids.com
stop4kids.org
us-core.com
vivrea.co
vivrea.us
"))

(define virtual-mail-recipients
  (plain-file
   "virtual-mail-recipients"
   "
felix.lechner               lechner
kate.wu                     wu

root                        lechner

mailer-daemon               root
postmaster                  root
nobody                      root
hostmaster                  root
usenet                      root
news                        root
webmaster                   root
www                         root
ftp                         root
abuse                       root
noc                         root
security                    root
clamav                      root
logcheck                    root
apt-mirror                  root
backup                      root
"))

(define debbugs-recipients-regex
  (plain-file
   "debbugs-recipients-regex"
   "
^submit@patchwise\\.org$
^control@patchwise\\.org$
^quiet@patchwise\\.org$
^[0-9]+(-(done|close|quiet))?@patchwise\\.org$
"))

(define gnu-recipients
  (plain-file
   "gnu-recipients"
   "
adns-discuss@patchwise.org
bug-auctex@patchwise.org
bug-automake@patchwise.org
automake-patches@patchwise.org
bug-coreutils@patchwise.org
bug-cppi@patchwise.org
bug-dejagnu@patchwise.org
bug-diffutils@patchwise.org
bug-gnu-emacs@patchwise.org
ada-mode-users@patchwise.org
org-mode@patchwise.org
bug-anubis@patchwise.org
bug-librefm@patchwise.org
bug-gnuzilla@patchwise.org
bug-grep@patchwise.org
bug-guile@patchwise.org
bug-guix@patchwise.org
guix-patches@patchwise.org
bug-gzip@patchwise.org
bug-hyperbole@patchwise.org
bug-idutils@patchwise.org
bug-libtool@patchwise.org
bug-mumi@patchwise.org
bug-parted@patchwise.org
bug-sed@patchwise.org
bug-vc-dwim@patchwise.org
"))

(define nongnu-recipients
  (plain-file
   "nongnu-recipients"
   "
skribilo-bugs@patchwise.org
woodchuck-bugs@patchwise.org
"))

(define public-mailing-lists
  (plain-file
   "public-mailing-lists"
   "
help@patchwise.org
"))

(define local-mailing-lists
  (plain-file
   "local-mailing-lists"
   "
bugs-to-scan@patchwise.org
debbugs-configuration-changes-rsync@patchwise.org
debbugs-state-changes-rsync@patchwise.org
"))

(define (borg-backup-shepherd-service config)
  (shepherd-service
   (provision '(borg-backup))
   (requirement '(file-systems user-processes))
   (modules '((ice-9 format)
              (ice-9 rdelim)
              (shepherd service timer)))
   (start #~(make-timer-constructor
             ;; Run at 2AM every day
             (calendar-event #:hours '(2))
             (lambda _
               (define borg-command
                 (list #$(file-append borg "/bin/borg") "--remote-path=borg1"))

               (define borg-repo
                 "ssh://sd1064@sd1064.rsync.net/~/backup/borg")

               ;; some helpers and error handling
               (define (info message)
                 (let ((timestamp (strftime "%Y-%m-%dT%H:%M:%S%zZ" (localtime (current-time)))))
                   (format (current-error-port) "\n~a ~a\n\n" timestamp message)))

               (let* ((backup-command
                       (append borg-command
                               ;; back up the most important directories into an archive named after
                               ;; the machine this script is currently running on
                               (list "create"
                                     "--verbose"
                                     "--filter" "AME"
                                     "--list"
                                     "--stats"
                                     "--show-rc"
                                     "--exclude-caches"
                                     "--exclude" "*/.Trash*"
                                     "--exclude" "*/Share"
                                     "--exclude" "*/lost+found"
                                     "--exclude" "*/bagguley"
                                     "--exclude" "*/congdon"
                                     "--exclude" "*/xiaojun"
                                     (string-append borg-repo "::{hostname}-{now}")
                                     "/srv/nfs/acct/")))
                      (prune-command
                       (append borg-command
                               ;; use the 'prune' subcommand to maintain 7 daily, 4 weekly and 6 monthly
                               ;; archives of THIS machine. The '{hostname}-' prefix is very important to
                               ;; limit prune's operation to this machine's archives and not apply to
                               ;; other machines' archives, as well
                               (list "prune"
                                     "--list"
                                     "--glob-archives" "{hostname}-*"
                                     "--show-rc"
                                     "--keep-daily" "7"
                                     "--keep-weekly" "4"
                                     "--keep-monthly" "6"
                                     borg-repo)))
                      (compact-command
                       (append borg-command
                               ;; free up disk space
                               (list "compact"
                                     borg-repo)))
                      (backup-status
                       (begin
                         (info "Starting backup.")
                         (apply system* backup-command)))
                      (backup-signal (status:term-sig backup-status))
                      (backup-exit-value (status:exit-val backup-status)))
                 (if backup-signal
                     (info (string-append "Backup interrupted by signal "
                                          (number->string backup-signal) "."))
                     (let* ((prune-status
                             (begin
                               (info "Pruning repository.")
                               (apply system* prune-command)))
                            (prune-signal (status:term-sig prune-status))
                            (prune-exit-value (status:exit-val prune-status)))
                       (if prune-signal
                           (info (string-append "Pruning interrupted by signal "
                                                (number->string prune-signal) "."))
                           (let* ((compact-status
                                   (begin
                                     (info "Compacting repository.")
                                     (apply system* compact-command)))
                                  (compact-signal (status:term-sig compact-status))
                                  (compact-exit-value (status:exit-val compact-status)))
                             (if compact-signal
                                 (info (string-append "Compacting interrupted by signal "
                                                      (number->string compact-signal) "."))
                                 ;; use highest exit code as global exit code
                                 (let ((result (max backup-exit-value prune-exit-value compact-exit-value)))
                                   (case result
                                     ((0) (info "Backup, Prune and Compact finished successfully."))
                                     ((1) (info "Backup, Prune or Compact finished with warnings."))
                                     ((else) (info "Backup, Prune or Compact finished with errors.")))
                                   (quit result)))))))))))
   (stop #~(make-timer-destructor))
   (actions
    (list (shepherd-action
           (name 'trigger)
           (documentation "Trigger the action associated with this timer.")
           (procedure #~trigger-timer))))
   (documentation "Borg backup")))

(define borg-backup-service-type
  (service-type
   (name 'borg-backup)
   (description
    "Borg backup")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list borg-backup-shepherd-service))))
   (default-value #f)))

(define-maybe string)

(define (serialize-raw-contents variable-symbol text)
  (if (maybe-value-set? text)
      #~(format #f "~a~%" #$text)
      ""))

(define-configuration btrbk-configuration
  (btrbk
   (file-like btrbk)
   "The btrbk package to use."
   (serializer empty-serializer))

  (raw
   maybe-string
   "Raw contents of configuration file."
   (serializer serialize-raw-contents)))

(define (serialize-btrbk-configuration configuration)
  (mixed-text-file
   "btrbk.conf"
   (serialize-configuration configuration btrbk-configuration-fields)))

(define (btrbk-snapshot-shepherd-service config)
  (match-record
      config <btrbk-configuration> (btrbk)
      (let ((configuration-file (serialize-btrbk-configuration config)))
        (shepherd-service
         (provision '(btrbk-snapshot))
         (requirement '(file-systems user-processes))
         (modules '((shepherd service timer)))
         (start #~(make-timer-constructor
                   ;; Run at two minutes past every hour
                   (calendar-event #:minutes '(2))
                   (command (list #$(file-append btrbk "/bin/btrbk")
                                  (string-append "--config=" #$configuration-file)
                                  "--format=table"
                                  "run"))))
         (stop #~(make-timer-destructor))
         (actions
          (list (shepherd-action
                 (name 'trigger)
                 (documentation "Trigger the action associated with this timer.")
                 (procedure #~trigger-timer))))
         (documentation "Btrbk snapshot")))))

(define btrbk-snapshot-service-type
  (service-type
   (name 'btrbk-snapshot)
   (description
    "Btrbk snapshot")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list btrbk-snapshot-shepherd-service))))
   (default-value #f)))

(define (mdadm-resync-shepherd-service config)
  (shepherd-service
   (provision '(mdadm-resync))
   (requirement '(file-systems user-processes))
   (modules '((ice-9 ftw)
              (ice-9 regex)
              (shepherd service timer)))
   (start #~(make-timer-constructor
             ;; Every first Sunday of the month at 1 AM.
             (calendar-event #:days-of-month '(1 2 3 4 5 6 7)
                             #:days-of-week '(sunday)
                             #:hours '(1))
             (lambda _
               ;; some helpers and error handling
               (define (info message)
                 (let ((timestamp (strftime "%Y-%m-%dT%H:%M:%S%zZ" (localtime (current-time)))))
                   (format (current-error-port) "~a ~a~%" timestamp message)))

               (define (resync array)
                 (let ((port (open-output-file (string-append "/sys/block/" array "/md/sync_action"))))
                   (display "check" port)
                   (close-port port))
                 (info (string-append "Started MD resync for " array ".")))

               (let* ((is-mdadm-device? (lambda (file)
                                          (string-match "^md.+" file)))
                      (arrays (scandir "/dev" is-mdadm-device?)))
                 (map resync arrays)))))
   (stop #~(make-timer-destructor))
   (actions
    (list (shepherd-action
           (name 'trigger)
           (documentation "Trigger the action associated with this timer.")
           (procedure #~trigger-timer))))
   (documentation "MD array resync")))

(define mdadm-resync-service-type
  (service-type
   (name 'mdadm-resync)
   (description "MD array resync")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list mdadm-resync-shepherd-service))))
   (default-value #f)))

(define (garbage-collector-shepherd-service config)
  (shepherd-service
   (provision '(garbage-collector))
   (requirement '(guix-daemon))
   (modules '((shepherd service timer)))
   (start #~(make-timer-constructor
             ;; Five minutes after midnight every day.
             (calendar-event #:hours '(0) #:minutes '(5))
             (command (list #$(file-append guix "/bin/guix")
                            "gc"
                            "--free-space=1G"))))
   (stop #~(make-timer-destructor))
   (actions
    (list (shepherd-action
           (name 'trigger)
           (documentation "Trigger the action associated with this timer.")
           (procedure #~trigger-timer))))
   (documentation "Maintain minimum free space by cleaning up Guix garbage")))

(define garbage-collector-service-type
  (service-type
   (name 'garbage-collector)
   (description
    "Maintain minimum free space by cleaning up Guix garbage")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list garbage-collector-shepherd-service))))
   (default-value #f)))

(define (rsync-debbugs-config-shepherd-service config)
  (shepherd-service
   (provision '(rsync-debbugs-config))
   (requirement '(networking file-systems))
   (modules '((ice-9 popen)
              (ice-9 textual-ports)
              (shepherd service timer)
              (srfi srfi-1)))
   (start #~(make-timer-constructor
             ;; Run every five minutes
             (calendar-event #:minutes (iota 12 2 5))
             (lambda _
               (let* ((rsync-port
                       (open-pipe* OPEN_READ
                                   #$(file-append rsync "/bin/rsync")
                                   "-4"
                                   "--delete"
                                   "-a"
                                   "--chown=patchwise:patchwise"
                                   "--itemize-changes"
                                   "--password-file=/srv/patchwise/secrets/etc-debbugs.rsync"
                                   "debbugs-etc@debbugs.gnu.org::etc-debbugs"
                                   "/srv/patchwise/debbugs.gnu.org/config/"))
                      (rsync-changes (get-string-all rsync-port)))
                 (close-port rsync-port)
                 (if (string-null? (string-trim rsync-changes))
                     #f
                     (let ((to '("debbugs-configuration-changes-rsync@patchwise.org"))
                           (from (string-append "Rsync job <root@" (gethostname) ">"))
                           (subject "Itemized changes for Debbugs configuration")
                           (email-port (open-pipe* OPEN_WRITE "/run/setuid-programs/sendmail" "-t")))
                       (format email-port "To: ~a~%" (string-join to ", "))
                       (format email-port "From: ~a~%" from)
                       (format email-port "Subject: ~a~%" subject)
                       (format email-port "~%")
                       (format email-port "~a" rsync-changes)
                       (close-port email-port)))))))
   (stop #~(make-timer-destructor))
   (actions
    (list (shepherd-action
           (name 'trigger)
           (documentation "Trigger the action associated with this timer.")
           (procedure #~trigger-timer))))
   (documentation "Timer to rsync Debbugs configuration to this machine.")))

(define rsync-debbugs-config-service-type
  (service-type
   (name 'rsync-debbugs-config)
   (description
    "Rsync Debbugs configuration to this machine.")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list rsync-debbugs-config-shepherd-service))))
   (default-value #f)))

(define (rsync-debbugs-state-shepherd-service config)
  (shepherd-service
   (provision '(rsync-debbugs-state))
   (requirement '(networking file-systems))
   (modules '((ice-9 popen)
              (ice-9 rdelim)
              (ice-9 regex)
              (ice-9 textual-ports)
              (shepherd service timer)
              (srfi srfi-1)
              (srfi srfi-158)))
   (start
    (with-extensions (list guile-srfi-158)
      #~(make-timer-constructor
         ;; Run every five minutes
         (calendar-event #:minutes (iota 12 3 5))
         (lambda _
           (let* ((rsync-port
                   (open-pipe* OPEN_READ
                               #$(file-append rsync "/bin/rsync")
                               "-4"
                               "--delete"
                               "-a"
                               "--chown=patchwise:patchwise"
                               "--itemize-changes"
                               "--password-file=/srv/patchwise/secrets/var-lib-debbugs.rsync"
                               "debbugs-var@debbugs.gnu.org::var-lib-debbugs"
                               "/srv/patchwise/debbugs.gnu.org/state/"))
                  (rsync-changes (get-string-all rsync-port)))
             (close-port rsync-port)
             (if (string-null? (string-trim rsync-changes))
                 #f
                 (let ((to '("debbugs-state-changes-rsync@patchwise.org"))
                       (from (string-append "Rsync job <root@" (gethostname) ">"))
                       (subject "Itemized changes for Debbugs state")
                       (email-port (open-pipe* OPEN_WRITE "/run/setuid-programs/sendmail" "-t")))
                   (format email-port "To: ~a~%" (string-join to ", "))
                   (format email-port "From: ~a~%" from)
                   (format email-port "Subject: ~a~%" subject)
                   (format email-port "~%")
                   (format email-port "~a" rsync-changes)
                   (close-port email-port)))
             (let* ((changes-alist
                     (filter identity
                             (call-with-input-string rsync-changes
                               (lambda (port)
                                 (generator->list
                                  (make-unfold-generator
                                   eof-object?
                                   (lambda (port)
                                     (let* ((line (read-line port)))
                                       (if (eof-object? line)
                                           line
                                           (if (char=? #\sp (string-ref line 11))
                                               (let ((filename (string-drop line 12))
                                                     (change (string-trim-right (string-take line 11))))
                                                 (cons filename change))
                                               (begin
                                                 (error "Cannot analyze line: ~a" line)
                                                 #f)))))
                                   values
                                   port))))))
                    (changed-bugs-alist
                     (filter-map (lambda (cell)
                                   (let ((key (car cell))
                                         (value (cdr cell)))
                                     (if (string-match "^>f" value)
                                         (let ((match-result
                                                (string-match
                                                 "^spool/(db-h|archive)/([0-9]{1,2})/([0-9]{1,})[.]log$"
                                                 key)))
                                           (if match-result
                                               (let* ((fork (match:substring match-result 1))
                                                      (sorter (match:substring match-result 2))
                                                      (bug (match:substring match-result 3))
                                                      (path (string-append "spool/" fork "/" sorter)))
                                                 (cons bug path))
                                               #f))
                                         #f)))
                                 changes-alist)))
               (if (null? changed-bugs-alist)
                   #f
                   (let* ((bugs (map car changed-bugs-alist))
                          (sorted (sort bugs
                                        (lambda (a b)
                                          (< (string->number a) (string->number b))))))
                     (for-each (lambda (bug)
                                 (let* ((path (assoc-ref changed-bugs-alist bug))
                                        (summary-file (string-append "/srv/patchwise/debbugs.gnu.org/state/" path "/" bug ".summary"))
                                        (summary-alist
                                         (call-with-input-file summary-file
                                           (lambda (port)
                                             (let loop ((alist '()))
                                               (let ((line (read-line port)))
                                                 (if (eof-object? line) alist
                                                     (let* ((split-point (string-index line #\:))
                                                            (key (string-take line split-point))
                                                            (value (string-drop line (1+ split-point))))
                                                       (loop (cons (cons key
                                                                         (string-trim-both value))
                                                                   alist)))))))))
                                        (package (assoc-ref summary-alist "Package"))
                                        (title (assoc-ref summary-alist "Subject")))
                                   (call-with-input-file summary-file
                                     (lambda (port)
                                       (let ((to '("bugs-to-scan@patchwise.org"))
                                             (from (string-append "Rsync job <root@" (gethostname) ">"))
                                             (subject (format #f "~a: Bug#~a: ~a" package bug title))
                                             (email-port (open-pipe* OPEN_WRITE "/run/setuid-programs/sendmail" "-t")))
                                         (format email-port "To: ~a~%" (string-join to ", "))
                                         (format email-port "From: ~a~%" from)
                                         (format email-port "Subject: ~a~%" subject)
                                         (format email-port "~%")
                                         (format email-port "~a" (get-string-all port))
                                         (close-port email-port))))))
                               ;;           sorted)
                               ;; (for-each (lambda (bug)
                               ;;             (let* ((path (assoc-ref changed-bugs-alist bug))
                               ;;                    ;(path (assoc-ref details-alist 'path))
                               ;;                    ;(fork (assoc-ref details-alist 'fork))
                               ;;                    (summary-file (string-append "/srv/patchwise/debbugs.gnu.org/state/" path "/" bug ".summary"))
                               ;;                    (summary-alist
                               ;;                     (call-with-input-file summary-file
                               ;;                       (lambda (port)
                               ;;                         (let loop ((alist '()))
                               ;;                           (let ((line (read-line port)))
                               ;;                             (if (eof-object? line) alist
                               ;;                                 (let* ((split-point (string-index line #\:))
                               ;;                                        (key (string-take line split-point))
                               ;;                                        (value (string-drop line (1+ split-point))))
                               ;;                                   (loop (cons (cons key
                               ;;                                                     (string-trim-both value))
                               ;;                                               alist)))))))))
                               ;;                    (package (assoc-ref summary-alist "Package"))
                               ;;                    (title (assoc-ref summary-alist "Subject")))
                               ;;               (if (file-exists? "/srv/patchwise/irc/irc.libera.chat/#patchwise/in")
                               ;;                   (with-output-to-file "/srv/patchwise/irc/irc.libera.chat/#patchwise/in"
                               ;;                     (lambda _
                               ;;                       (format #t "Something's happening in issue #~a.~%" bug))))))
                               sorted)))))))))
   (stop #~(make-timer-destructor))
   (actions
    (list (shepherd-action
           (name 'trigger)
           (documentation "Trigger the action associated with this timer.")
           (procedure #~trigger-timer))))
   (documentation "Timer to rsync Debbugs bug details to this machine.")))

(define rsync-debbugs-state-service-type
  (service-type
   (name 'rsync-debbugs-state)
   (description
    "Rsync Debbugs bug details to this machine.")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list rsync-debbugs-state-shepherd-service))))
   (default-value #f)))

(define (socat-shepherd-service config)
  "Return a <shepherd-service> for socat using CONFIG."
  (let* ((server "irc.libera.chat")
         (port 6697)
         (nick "walnut")
         (irc-folder "/srv/patchwise/irc")
         (nick-folder (string-join (list irc-folder nick) "/"))
         (socket-file (string-join (list nick-folder
                                         (string-append server ".socket"))
                                   "/"))
         (certificate-file (string-join (list irc-folder "libera.pem") "/"))
         (user "patchwise")
         (group "patchwise"))
    (shepherd-service
     (documentation "Run a socat process for network communications.")
     (provision (list (string->symbol
                       (string-append "socat-" socket-file))))
     (requirement '(file-systems user-processes networking))
     (start #~(let* ((uid (passwd:uid (getpwnam #$user)))
                     (gid (group:gid (getgrnam #$group))))
                (mkdir-p #$irc-folder)
                (chown #$irc-folder uid gid)
                (mkdir-p #$nick-folder)
                (chown #$nick-folder uid gid)
                (make-forkexec-constructor
                 `(#$(file-append socat "/bin/socat")
                   ,(string-join (list
                                  (string-append "UNIX-LISTEN:" #$socket-file)
                                  (string-append "user=" #$user)
                                  (string-append "group=" #$group))
                                 ",")
                   ,(string-join (list
                                  (string-append "OPENSSL:" #$server ":" (number->string #$port))
                                  (string-append "cert=" #$certificate-file))
                                 ",")))))
     (stop #~(make-kill-destructor)))))

(define (ii-client-shepherd-service config)
  "Return a <shepherd-service> for an ii IRC client using CONFIG."
  (let* ((server "irc.libera.chat")
         (nick "walnut")
         (socket-file "/srv/patchwise/irc/libera-debbugs.socket")
         (port "6666")
         (interaction-folder "/srv/patchwise/irc")
         (user "patchwise")
         (group "patchwise"))
    (shepherd-service
     (documentation "Run an ii IRC client.")
     (provision (list (string->symbol
                       (string-append "ii-client-" server ":" nick))))
     (requirement '(file-systems user-processes networking))
     (start #~(make-forkexec-constructor
               `(#$(file-append ii "/bin/ii")
                 "-s" #$server
                 "-n" #$nick
                 "-p" #$port
                 "-i" #$interaction-folder)
               #:user #$user
               #:group #$group))
     (stop #~(make-kill-destructor)))))

(define ii-client-service-type
  (service-type
   (name 'ii-client)
   (description
    "Run an ii client connecting to IRC.")
   (extensions
    (list (service-extension
           shepherd-root-service-type
           (lambda (config)
             (list
              (ii-client-shepherd-service config))))))
   (default-value #f)))

(define (public-inbox-nntpd-shepherd-service config)
  (shepherd-service
   (provision '(public-inbox-nntpd))
   (requirement '(networking file-systems))
   (start #~(make-systemd-constructor
             (list #$(file-append public-inbox "/bin/public-inbox-nntpd")
                   "--cert" "/srv/patchwise/certs/fullchain.pem"
                   "--key" "/srv/patchwise/certs/privkey.pem")
             (list (endpoint (make-socket-address AF_INET INADDR_ANY 119))
                   (endpoint (make-socket-address AF_INET6 IN6ADDR_ANY 119)))
             #:user "patchwise"
             #:group "patchwise"
             #:environment-variables
             (append (list
                      "PI_CONFIG=/srv/patchwise/public-inbox/config")
                     (default-environment-variables))))
   (stop #~(make-systemd-destructor))
   (documentation "Start 'public-inbox-nntpd', the NNTP daemon from
public-inbox.org.")))

(define public-inbox-nntpd-service-type
  (service-type
   (name 'public-inbox-nntpd)
   (description
    "Run the public-inbox NNTP daemon @command{public-inbox-nntpd}.")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list public-inbox-nntpd-shepherd-service))))
   (default-value #f)))

(define (public-inbox-imapd-shepherd-service config)
  (shepherd-service
   (provision '(public-inbox-imapd))
   (requirement '(networking file-systems))
   (start #~(make-systemd-constructor
             (list #$(file-append public-inbox "/bin/public-inbox-imapd")
                   "--cert" "/srv/patchwise/certs/fullchain.pem"
                   "--key" "/srv/patchwise/certs/privkey.pem")
             (list (endpoint (make-socket-address AF_INET INADDR_ANY 143))
                   (endpoint (make-socket-address AF_INET6 IN6ADDR_ANY 143)))
             #:user "patchwise"
             #:group "patchwise"
             #:environment-variables
             (append (list
                      "PI_CONFIG=/srv/patchwise/public-inbox/config")
                     (default-environment-variables))))
   (stop #~(make-systemd-destructor))
   (documentation "Start 'public-inbox-imapd', the IMAP daemon from
public-inbox.org.")))

(define public-inbox-imapd-service-type
  (service-type
   (name 'public-inbox-imapd)
   (description
    "Run the public-inbox IMAP daemon @command{public-inbox-imapd}.")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list public-inbox-imapd-shepherd-service))))
   (default-value #f)))

(define (public-inbox-httpd-shepherd-service config)
  (shepherd-service
   (provision '(public-inbox-httpd))
   (requirement '(networking file-systems))
   (start #~(make-systemd-constructor
             (list #$(file-append public-inbox "/bin/public-inbox-httpd"))
             (list (endpoint (make-socket-address AF_INET INADDR_ANY 1235))
                   (endpoint (make-socket-address AF_INET6 IN6ADDR_ANY 1235)))
             #:user "patchwise"
             #:group "patchwise"
             #:environment-variables
             (append (list
                      "PI_CONFIG=/srv/patchwise/public-inbox/config")
                     (default-environment-variables))))
   (stop #~(make-systemd-destructor))
   (documentation "Start 'public-inbox-httpd', the HTTP daemon from
public-inbox.org.")))

(define public-inbox-httpd-service-type
  (service-type
   (name 'public-inbox-httpd)
   (description
    "Run the public-inbox IMAP daemon @command{public-inbox-httpd}.")
   (extensions
    (list
     (service-extension shepherd-root-service-type
                        (compose list public-inbox-httpd-shepherd-service))))
   (default-value #f)))

(define %public-inbox-deploy-hook
  (program-file
   "public-inbox-deploy-hook"
   #~(let ((patchwise-certs "/etc/letsencrypt/live/patchwise.org/")
           (public-inbox-certs "/srv/patchwise/certs/")
           (uid (passwd:uid (getpwnam "patchwise")))
           (gid (group:gid (getgrnam "patchwise"))))
       (copy-file (string-append patchwise-certs "fullchain.pem") public-inbox-certs)
       (chown (string-append public-inbox-certs "fullchain.pem") uid gid)
       (copy-file (string-append patchwise-certs "privkey.pem") public-inbox-certs)
       (chown (string-append public-inbox-certs "privkey.pem") uid gid)
       (herd restart public-inbox-nntpd)
       (herd restart public-inbox-imapd))))

(define %prosody-deploy-hook
  (program-file
   "prosody-deploy-hook"
   #~(let ((lease-up-certs "/etc/letsencrypt/live/lease-up.com/")
           (prosody-certs "/etc/prosody/certs/")
           (uid (passwd:uid (getpwnam "prosody")))
           (gid (group:gid (getgrnam "prosody"))))
       (copy-file (string-append lease-up-certs "fullchain.pem") prosody-certs)
       (chown (string-append prosody-certs "fullchain.pem") uid gid)
       (copy-file (string-append lease-up-certs "privkey.pem") prosody-certs)
       (chown (string-append prosody-certs "privkey.pem") uid gid)
       (herd restart prosody))))

(define %smtpd-deploy-hook
  (program-file "smtpd-deploy-hook" (gexp (herd restart smtpd))))

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(define nginx-fastcgi-defaults
  (plain-file "fastcgi.conf"
              "
fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
fastcgi_param  SERVER_SOFTWARE    nginx;
fastcgi_param  QUERY_STRING       $query_string;
fastcgi_param  REQUEST_METHOD     $request_method;
fastcgi_param  CONTENT_TYPE       $content_type;
fastcgi_param  CONTENT_LENGTH     $content_length;
fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
fastcgi_param  REQUEST_URI        $request_uri;
fastcgi_param  DOCUMENT_URI       $document_uri;
fastcgi_param  DOCUMENT_ROOT      $document_root;
fastcgi_param  SERVER_PROTOCOL    $server_protocol;
fastcgi_param  REMOTE_ADDR        $remote_addr;
fastcgi_param  REMOTE_PORT        $remote_port;
fastcgi_param  SERVER_ADDR        $server_addr;
fastcgi_param  SERVER_PORT        $server_port;
fastcgi_param  SERVER_NAME        $server_name;"))

(operating-system
  (host-name "wallace-server")
  (timezone "America/Los_Angeles")
  (locale "en_US.utf8")

  (kernel linux)
  (kernel-arguments (cons "consoleblank=60"
                          %default-kernel-arguments))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  (keyboard-layout (keyboard-layout "us" "altgr-intl"))

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets (list "/efi"))
               (keyboard-layout keyboard-layout)))

  (initrd-modules (append (list "mpt3sas" "raid1") %base-initrd-modules))

  ;; Specify a mapped device for the encrypted root partition.
  ;; The UUID is that returned by 'cryptsetup luksUUID'.
  (mapped-devices
   (list

    (mapped-device
     (source (uuid "33cf3e31:8e33d75b:537d64b9:0a8f7621" 'mdadm))
     (target "/dev/md12")
     (type md-array-device-mapping))

    (mapped-device
     (source "wallacevg")
     (targets (list "wallacevg-guix"))
     (type lvm-device-mapping))))

  (file-systems
   (append (list

            (file-system
              (device (file-system-label "GUIX"))
              (mount-point "/")
              (type "ext4")
              (dependencies mapped-devices))

            (file-system
              (device (file-system-label "WALLACE-BTRFS"))
              (mount-point "/srv/nfs/acct")
              (type "btrfs")
              (options "subvol=@acct,compress=lzo")
              (dependencies mapped-devices))

            (file-system
              (device (file-system-label "WALLACE-BTRFS"))
              (mount-point "/history/btrfs")
              (type "btrfs")
              (options "subvol=@history")
              (dependencies mapped-devices))

            (file-system
              (device (file-system-label "PATCHWISE"))
              (mount-point "/srv/patchwise")
              (type "ext4")
              (dependencies mapped-devices))

            (file-system
              (device (file-system-label "EFI_6GLY8WR"))
              (mount-point "/efi")
              (type "vfat")))

           %base-file-systems))

  (swap-devices (list

                 (swap-space
                   (target (file-system-label "swap"))
                   (dependencies mapped-devices))))

  (users
   (append (list

            (user-account
             (system? #true)
             (name "_dkimsign")
             (group "_dkimsign")
             (home-directory "/etc/mail/dkim")))

           (load "../../users.scm")

           %base-user-accounts))

  (groups
   (append (list

            (user-group
             (system? #true)
             (name "_dkimsign")))

           (load "../../groups.scm")

           %base-groups))

  (packages
   (append (list

            atftp
            bcachefs-tools
            borg
            btrbk
            certbot
            dovecot
            efibootmgr
            gocryptfs
            gptfdisk
            hdparm
            heimdal
            lsscsi
            lvm2
            mdadm
            mumi
            net-tools
            nfs-utils
            openssl
            prosody
            public-inbox
            rsync
            sdparm
            sg3-utils
            smartmontools)

           %base-packages))

  (services
   (append (list

            (service irc-helper-bot-service-type/juix
                     (irc-helper-bot-configuration/juix
                      (nick "peanuts")
                      (channels '("#guix"))
                      (server "irc.libera.chat")
                      (port 6697)
                      (user "patchwise")
                      (group "patchwise")
                      (reply "Hi, for comments please contact my maintainers at https://codeberg.org/lechner/irc-helper-bot")))

            (service heimdal-kdc-service-type)
            (service heimdal-kadmind-service-type)

            (service postgresql-service-type
                     (postgresql-configuration
                      (postgresql postgresql-15)
                      (config-file
                       (postgresql-config-file
                        (hba-file
                         (plain-file "pg_hba.conf"
                                     "
local   all     all                     trust
host    all     all     127.0.0.1/32    trust
host    all     all     ::1/128         trust
host    all     all     10.0.0.0/8      scram-sha-256
host    all     all     fc00::/7        scram-sha-256"))
                        (extra-config
                         '(("listen_addresses" "*")
                           ("password_encryption" "scram-sha-256")
                           #;("ssl_cert_file" "/etc/letsencrypt/live/sail-ipv4.us-core.com/fullchain.pem")
                           #;("ssl_key_file" "/etc/letsencrypt/live/sail-ipv4.us-core.com/privkey.pem")
                           ))))))

            (service nfs-service-type
                     (nfs-configuration
                      (exports
                       (list
                        (list "/srv/nfs" "*(rw,no_subtree_check,sec=sys:krb5i:krb5p,fsid=0)")
                        (list "/srv/nfs/acct" "*(rw,no_subtree_check,sec=sys:krb5i:krb5p,mountpoint)")))))

            (service samba-service-type
                     (samba-configuration
                      (enable-smbd? #t)
                      (config-file (plain-file "smb.conf" "\
[global]
server min protocol = NT1
map to guest = Bad User
logging = syslog@1

[public]
browsable = yes
path = /public
read only = no
guest ok = yes
guest only = yes
"))))

            ;; for gocryptfs
            (load "../../service/pam-limits.scm")
            (load "../../service/openssh.scm")
            (load "../../service/krb5-association.scm")
            (load "../../service/cups.scm")
            (load "../../service/udev-rules-net-name-mac.scm")

            (service knot-service-type
                     (knot-configuration
                      (remotes our-slave-servers)
                      (acls our-acls)
                      (zones master-zones)))

            (service fcgiwrap-service-type
                     (fcgiwrap-configuration
                      (socket "tcp:127.0.0.1:3012")
                      (user "patchwise")
                      (group "patchwise")))

            (service nginx-service-type
                     (nginx-configuration
                      (extra-content (list "http2 on;"))

                      (server-blocks
                       (list

                        (nginx-server-configuration
                         (server-name (list "28palms.org" "www.28palms.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/28palms.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/28palms.org/privkey.pem")
                         (root "/srv/www/28palms.org/static"))

                        (nginx-server-configuration
                         (server-name (list "dazzledigs.com" "www.dazzledigs.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/dazzledigs.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/dazzledigs.com/privkey.pem")
                         (root "/srv/www/dazzledigs.com/static"))

                        ;; redirect to dazzledigs.com
                        (nginx-server-configuration
                         (server-name (list "dazzledigs.org" "www.dazzledigs.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/dazzledigs.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/dazzledigs.org/privkey.pem")
                         (raw-content (list "return 302 https://dazzledigs.com$request_uri;")))

                        ;; redirect to dazzledigs.com
                        (nginx-server-configuration
                         (server-name (list "dazzledigs.net" "www.dazzledigs.net"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/dazzledigs.net/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/dazzledigs.net/privkey.pem")
                         (raw-content (list "return 302 https://dazzledigs.com$request_uri;")))

                        ;; redirect to innovametro
                        (nginx-server-configuration
                         (server-name (list "amegrund.com" "www.amegrund.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/amegrund.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/amegrund.com/privkey.pem")
                         (raw-content (list "return 302 https://innovametro.com$request_uri;")))

                        ;; redirect to innovametro
                        (nginx-server-configuration
                         (server-name (list "lease-up.com" "www.lease-up.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/lease-up.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/lease-up.com/privkey.pem")
                         (raw-content (list "return 302 https://innovametro.com$request_uri;")))

                        (nginx-server-configuration
                         (server-name (list "contacts.lease-up.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/contacts.lease-up.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/contacts.lease-up.com/privkey.pem")
                         (root "/srv/www/contacts.lease-up.com")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/")
                                 (body (list
                                        "proxy_pass http://localhost:5232;"
                                        "proxy_set_header Host $host;"
                                        "proxy_set_header X-Real-IP $remote_addr;"
                                        "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                        "proxy_pass_header Authorization;"
                                        "proxy_connect_timeout 75;"
                                        "proxy_send_timeout 600;"
                                        "proxy_read_timeout 600;"))))))

                        ;; redirect to innovametro
                        (nginx-server-configuration
                         (server-name (list "us-core.com" "www.us-core.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/us-core.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/us-core.com/privkey.pem")
                         (raw-content (list "return 302 https://innovametro.com$request_uri;")))

                        (nginx-server-configuration
                         (server-name (list "fremontmip.org" "www.fremontmip.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/fremontmip.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/fremontmip.org/privkey.pem")
                         (index (list "index.php" "index.html" "index.htm"))
                         (root "/srv/www/fremontmip.org/wordpress")
                         (raw-content (list "client_max_body_size 64M;"))
                         (locations (list
                                     (nginx-location-configuration
                                      (uri "/")
                                      (body (list
                                             "try_files $uri $uri/ /index.php?$args;"))))))
                        ;;                                    (nginx-location-configuration
                        ;;                                     (uri "~ \.php$")
                        ;;                                     (body (list
                        ;;                                            "try_files $uri =404;"
                        ;;                                            "include /etc/nginx/fastcgi_params;"
                        ;;                                            "fastcgi_read_timeout 3600s;"
                        ;;                                            "fastcgi_buffer_size 128k;"
                        ;;                                            "fastcgi_buffers 4 128k;"
                        ;;                                            "fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;"
                        ;;                                            "fastcgi_pass unix:/run/php/php7.4-fpm.sock;"
                        ;;                                            "fastcgi_index index.php;"))))))

                        (nginx-server-configuration
                         (server-name (list "innovametro.com" "www.innovametro.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/innovametro.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/innovametro.com/privkey.pem")
                         (root "/srv/www/innovametro.com"))

                        (nginx-server-configuration
                         (server-name (list "juix.org" "www.juix.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/juix.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/juix.org/privkey.pem")
                         (root "/srv/www/juix.org"))

                        (nginx-server-configuration
                         (server-name (list "debbugs.juix.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/debbugs.juix.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/debbugs.juix.org/privkey.pem")
                         (root (file-append debbugs-gnu/juix "/share/html"))
                         (raw-content
                          (list "rewrite ^/(\\d+)$ /cgi/bugreport.cgi?bug=$1 redirect;"))
                         (locations
                          (list
                           (nginx-location-configuration
                            (uri "/css/")
                            (body (list
                                   #~(string-append "alias " #$(file-append debbugs-gnu/juix "/share/html/") ";"))))
                           (nginx-location-configuration
                            (uri "~ \\.cgi")
                            (body (list
                                   #~(string-append "root " #$(file-append debbugs-gnu/juix) ";")
                                   #~(string-append "include " #$nginx-fastcgi-defaults ";")
                                   "fastcgi_pass 127.0.0.1:3012;"))))))

                        (nginx-server-configuration
                         (server-name (list "debbugs-latest.juix.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/debbugs-latest.juix.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/debbugs-latest.juix.org/privkey.pem")
                         (root (file-append debbugs-gnu-experimental/juix "/share/html"))
                         (raw-content
                          (list "rewrite ^/(\\d+)$ /cgi/bugreport.cgi?bug=$1 redirect;"))
                         (locations
                          (list
                           (nginx-location-configuration
                            (uri "/css/")
                            (body (list
                                   #~(string-append "alias " #$(file-append debbugs-gnu-experimental/juix "/share/html/") ";"))))
                           (nginx-location-configuration
                            (uri "~ \\.cgi")
                            (body (list
                                   #~(string-append "root " #$(file-append debbugs-gnu-experimental/juix) ";")
                                   #~(string-append "include " #$nginx-fastcgi-defaults ";")
                                   "fastcgi_pass 127.0.0.1:3012;"))))))

                        (nginx-server-configuration
                         (server-name (list "find.juix.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/find.juix.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/find.juix.org/privkey.pem")
                         (root "/srv/www/find.juix.org")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/")
                                 (body (list
                                        "proxy_pass http://localhost:3011;"
                                        "proxy_set_header Host $host;"
                                        "proxy_set_header X-Real-IP $remote_addr;"
                                        "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                        "proxy_connect_timeout 75;"
                                        "proxy_send_timeout 600;"
                                        "proxy_read_timeout 600;"))))))

                        ;; redirect to patchwise.org
                        (nginx-server-configuration
                         (server-name (list "mumi.juix.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mumi.juix.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mumi.juix.org/privkey.pem")
                         (raw-content (list "return 302 https://www.patchwise.org$request_uri;")))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.28palms.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.28palms.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.28palms.org/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.amegrund.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.amegrund.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.amegrund.com/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.fremontmip.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.fremontmip.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.fremontmip.org/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.dazzledigs.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.dazzledigs.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.dazzledigs.com/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.dazzledigs.net"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.dazzledigs.net/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.dazzledigs.net/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.dazzledigs.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.dazzledigs.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.dazzledigs.org/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.innovametro.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.innovametro.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.innovametro.com/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.juix.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.juix.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.juix.org/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.lease-up.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.lease-up.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.lease-up.com/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.us-core.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.us-core.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.us-core.com/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.patchwise.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.patchwise.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.patchwise.org/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.stop4kids.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.stop4kids.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.stop4kids.com/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "mta-sts.stop4kids.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/mta-sts.stop4kids.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/mta-sts.stop4kids.org/privkey.pem")
                         (root "/srv/www/mta-sts")
                         (locations
                          (list (nginx-location-configuration
                                 (uri "/.well-known/mta-sts.txt")
                                 (body (list "try_files $uri $uri/ =404;"))))))

                        (nginx-server-configuration
                         (server-name (list "patchwise.org" "www.patchwise.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/patchwise.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/patchwise.org/privkey.pem")
                         (root "/srv/www/patchwise.org")
                         (locations
                          (list
                           (nginx-location-configuration
                            (uri "= /query/debbugs.wsdl")
                            (body (list
                                   #~(string-append "alias " #$(file-append patchwise/juix "/share/query/debbugs.wsdl") ";")
                                   "types { text/xml wsdl; }"
                                   "break;")))
                           (nginx-location-configuration
                            (uri "= /css/bugs.css")
                            (body (list
                                   #~(string-append "alias " #$(file-append debbugs-gnu-experimental/juix "/share/html/bugs.css") ";")
                                   "break;")))
                           (nginx-location-configuration
                            (uri "~ \\.cgi")
                            (body (list
                                   #~(string-append "root " #$(file-append debbugs-gnu-experimental/juix) ";")
                                   #~(string-append "include " #$nginx-fastcgi-defaults ";")
                                   "fastcgi_pass 127.0.0.1:3012;"
                                   "break;")))
                           (nginx-location-configuration
                            (uri "/inbox/")
                            (body (list
                                   "proxy_pass http://localhost:1235/;"
                                   "proxy_set_header Host $host;"
                                   "proxy_set_header X-Real-IP $remote_addr;"
                                   "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                   "proxy_connect_timeout 75;"
                                   "proxy_send_timeout 600;"
                                   "proxy_read_timeout 600;"
                                   "break;")))
                           (nginx-location-configuration
                            (uri "/")
                            (body (list
                                   "proxy_pass http://localhost:1234;"
                                   "proxy_set_header Host $host;"
                                   "proxy_set_header X-Real-IP $remote_addr;"
                                   "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                   "proxy_connect_timeout 75;"
                                   "proxy_send_timeout 600;"
                                   "proxy_read_timeout 600;"
                                   "break;"))))))

                        (nginx-server-configuration
                         (server-name (list "stop4kids.com" "www.stop4kids.com"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/stop4kids.com/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/stop4kids.com/privkey.pem")
                         (root "/srv/www/stop4kids.com/static"))

                        (nginx-server-configuration
                         (server-name (list "stop4kids.org" "www.stop4kids.org"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/stop4kids.org/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/stop4kids.org/privkey.pem")
                         (root "/srv/www/stop4kids.org/static"))

                        (nginx-server-configuration
                         (server-name (list "vivrea.co" "www.vivrea.co"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/vivrea.co/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/vivrea.co/privkey.pem")
                         (root "/srv/www/vivrea.co/static"))

                        (nginx-server-configuration
                         (server-name (list "vivrea.us" "www.vivrea.us"))
                         (listen '("443 ssl"))
                         (ssl-certificate "/etc/letsencrypt/live/vivrea.us/fullchain.pem")
                         (ssl-certificate-key "/etc/letsencrypt/live/vivrea.us/privkey.pem")
                         (root "/srv/www/vivrea.us/static"))))))

            (service certbot-service-type
                     (certbot-configuration
                      (email "felix.lechner@lease-up.com")
                      (certificates
                       (list

                        (certificate-configuration
                         (domains (list "mta-sts.28palms.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.amegrund.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.dazzledigs.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.dazzledigs.net"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.dazzledigs.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.fremontmip.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.innovametro.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.lease-up.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.patchwise.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.stop4kids.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.stop4kids.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mta-sts.us-core.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "28palms.org" "www.28palms.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "amegrund.com" "www.amegrund.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "dazzledigs.com" "www.dazzledigs.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "dazzledigs.net" "www.dazzledigs.net"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "dazzledigs.org" "www.dazzledigs.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "fremontmip.org" "www.fremontmip.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "lease-up.com" "www.lease-up.com"))
                         (deploy-hooks (list
                                        %nginx-deploy-hook
                                        %prosody-deploy-hook)))

                        (certificate-configuration
                         (domains (list "contacts.lease-up.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "us-core.com" "www.us-core.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "innovametro.com" "www.innovametro.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "juix.org"
                                        "www.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "debbugs.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "debbugs-latest.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "find.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "get.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "mumi.juix.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "patchwise.org" "www.patchwise.org"))
                         (deploy-hooks (list %public-inbox-deploy-hook
                                             %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "sail-ipv4.us-core.com"
                                        "imap.us-core.com"
                                        "submission.us-core.com"))
                         (deploy-hooks (list %smtpd-deploy-hook)))

                        (certificate-configuration
                         (domains (list "stop4kids.com" "www.stop4kids.com"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "stop4kids.org" "www.stop4kids.org"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "vivrea.co" "www.vivrea.co"))
                         (deploy-hooks (list %nginx-deploy-hook)))

                        (certificate-configuration
                         (domains (list "vivrea.us" "www.vivrea.us"))
                         (deploy-hooks (list %nginx-deploy-hook)))))))

            (service prosody-service-type
                     (prosody-configuration
                      (modules-enabled (cons* "admin_shell"
                                              "http_file_share"
                                              "mam"
                                              %default-modules-enabled))
                      (raw-content "
archive_expires_after = \"never\"
log = { warn = \"*syslog\"; }
")
                      (virtualhosts (list
                                     (virtualhost-configuration
                                      (domain "lease-up.com"))))
                      (ssl
                       (ssl-configuration
                        (certificate "/etc/prosody/certs/fullchain.pem")
                        (key "/etc/prosody/certs/privkey.pem")))))

            (service rspamd-service-type)

            (service opensmtpd-service-type
                     (opensmtpd-configuration
                      (shepherd-requirement '(networking))
                      (config-file
                       (mixed-text-file "opensmtpd-configuration"
                                        "
table client-secrets file:/etc/mail/client-secrets
table virtual-recipients file:" virtual-mail-recipients "
table local-domains file:" local-mail-domains "
table debbugs-recipients-regex file:" debbugs-recipients-regex "
table gnu-recipients file:" gnu-recipients "
table nongnu-recipients file:" nongnu-recipients "
table public-mailing-lists file:" public-mailing-lists "
table local-mailing-lists file:" local-mailing-lists "
table patchwise-virtuals { '@' = patchwise }

include \"/etc/mail/srs.conf\"

pki self cert \"/etc/letsencrypt/live/sail-ipv4.us-core.com/fullchain.pem\"
pki self key \"/etc/letsencrypt/live/sail-ipv4.us-core.com/privkey.pem\"

filter check_dyndns phase connect match rdns regex { '.*\\.dyn\\..*', '.*\\.dsl\\..*' } \
    disconnect \"550 no residential connections\"

filter check_rdns phase connect match !rdns \
    disconnect \"550 Reverse DNS required\"

filter check_fcrdns phase connect match !fcrdns \
    disconnect \"550 Forward-confirmed reverse DNS required\"

filter rspamd proc-exec \"" opensmtpd-filter-rspamd "/libexec/opensmtpd/filter-rspamd \"

filter dkimsign proc-exec \"" opensmtpd-filter-dkimsign "/libexec/opensmtpd/filter-dkimsign -d us-core.com -d lease-up.com -d innovametro.com -d amegrund.com -s 2017 -k /etc/mail/dkim/2017._domainkey.private.key\" user _dkimsign group _dkimsign

listen on enxb8ca3a875f15 port 25        hostname sail-ipv4.us-core.com pki self tls \
    filter { check_dyndns, check_rdns, check_fcrdns, rspamd }

listen on lo port 25          tag ORIGINATED_HERE hostname sail-ipv4.us-core.com mask-src filter dkimsign
listen on enxb8ca3a875f15 port 587       tag ORIGINATED_HERE hostname sail-ipv4.us-core.com mask-src filter dkimsign auth <client-secrets> pki self tls-require
listen on enxb8ca3a875f15 port 465       tag ORIGINATED_HERE hostname sail-ipv4.us-core.com mask-src filter dkimsign auth <client-secrets> pki self smtps
listen on socket              tag ORIGINATED_HERE mask-src filter dkimsign

action accept-for-user maildir \"/srv/nfs/acct/%{user.username}/wallace-server/Maildir\" junk alias <virtual-recipients>
action relay-for-user relay srs helo sail-ipv4.us-core.com
action redirect-to-debbugs mda \"/run/setuid-programs/sendmail -f %{sender} %{rcpt.user}@debbugs.gnu.org\" virtual <patchwise-virtuals>
action redirect-to-gnu mda \"/run/setuid-programs/sendmail -f %{sender} %{rcpt.user}@gnu.org\" virtual <patchwise-virtuals>
action redirect-to-nongnu mda \"/run/setuid-programs/sendmail -f %{sender} %{rcpt.user}@nongnu.org\" virtual <patchwise-virtuals>
action accept-for-list mda \"/usr/bin/env ORIGINAL_RECIPIENT=%{rcpt} PI_CONFIG=/srv/patchwise/public-inbox/config PI_EMERGENCY=/srv/patchwise/public-inbox/emergency " (file-append public-inbox "/bin/public-inbox-mda") "\" virtual <patchwise-virtuals> user patchwise 
action accept-for-project maildir \"/srv/patchwise/project/%{rcpt.user}/Maildir\" virtual <patchwise-virtuals>

match from any for rcpt-to regex <debbugs-recipients-regex> action redirect-to-debbugs
match from any for rcpt-to <gnu-recipients> action redirect-to-gnu
match from any for rcpt-to <nongnu-recipients> action redirect-to-nongnu
match from local for rcpt-to <local-mailing-lists> action accept-for-list
match from any for rcpt-to <public-mailing-lists> action accept-for-list
match from any for domain inbox.patchwise.org action accept-for-project
match from any for domain <local-domains> action accept-for-user
match from local for rcpt-to \"root@wallace-server\" action accept-for-user
match tag ORIGINATED_HERE from any for any action relay-for-user
match from local for any action relay-for-user
"))))

            (service radicale-service-type)

            (service dovecot-service-type
                     (dovecot-configuration
                      (mail-location "maildir:/srv/nfs/acct/%u/wallace-server/Maildir:LAYOUT=fs")
                      (listen '("*"))
                      (ssl? "required")
                      (ssl-cert "</etc/letsencrypt/live/sail-ipv4.us-core.com/fullchain.pem")
                      (ssl-key "</etc/letsencrypt/live/sail-ipv4.us-core.com/privkey.pem")
                      (passdbs (list
                                (passdb-configuration
                                 (driver "passwd-file")
                                 (args (list "scheme=sha512-crypt" "/etc/mail/imap.passwd")))))
                      (protocols (list
                                  (protocol-configuration
                                   (name "imap"))))
                      (services (list
                                 (service-configuration
                                  (kind "imap-login")
                                  (client-limit 0)
                                  (process-limit 0)
                                  (process-min-avail 4)
                                  (listeners
                                   (list
                                    (inet-listener-configuration (protocol "imap") (port 0) (ssl? #f))
                                    (inet-listener-configuration (protocol "imaps") (port 993) (ssl? #t)))))))))

            (service mumi-service-type
                     (mumi-configuration
                      (mumi mumi/local)
                      (user "patchwise")
                      (group "patchwise")
                      (work-folder "/srv/patchwise/mumi")
                      (debbugs-data-folder "/srv/patchwise/debbugs.gnu.org/state")
                      (mailer? #f)))

            (service ii-client-service-type)
            (service public-inbox-nntpd-service-type)
            (service public-inbox-imapd-service-type)
            (service public-inbox-httpd-service-type)

            (service garbage-collector-service-type)

            (service btrbk-snapshot-service-type
                     (btrbk-configuration
                      (raw "
timestamp_format long-iso
snapshot_preserve_min   72h
snapshot_preserve       96h 90d 52w 48m 5y

volume /history/btrfs
  snapshot_dir snapshots
  subvolume /srv/nfs/acct
")))

            (service mdadm-resync-service-type)
            (service borg-backup-service-type)

            (service rsync-debbugs-config-service-type)
            (service rsync-debbugs-state-service-type)

            (service ntp-service-type)
            (service avahi-service-type) ; for guix-publish

            (load "../../service/guix-publish.scm"))

           (load "../../services/greeter.scm")
           (load "../../services/desktop.scm")

           (modify-services %base-services

             (delete agetty-service-type)
             (delete mingetty-service-type)

             (guix-service-type config => (guix-configuration
                                           (inherit config)

                                           (extra-options (list
                                                           "--gc-keep-derivations=yes"
                                                           "--gc-keep-outputs=yes"))

                                           (substitute-urls
                                            (append (list "https://substitutes.nonguix.org")
                                                    %default-substitute-urls))
                                           (discover? #t)
                                           (authorized-keys
                                            (append (list
                                                     (plain-file "lechner-desktop.pub"
                                                                 "(public-key (ecc (curve Ed25519) (q #219656830F8DB4B7D45358F6AB9DE5FCD2877195D731A3B283A575983443C3DC#)))")
                                                     (plain-file "genenetwork.pub"
                                                                 "(public-key (ecc (curve Ed25519) (q #9578AD6CDB23BA51F9C4185D5D5A32A7EEB47ACDD55F1CCB8CEE4E0570FBF961#)))")
                                                     (plain-file "non-guix.pub"
                                                                 "(public-key (ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))
                                                    %default-authorized-guix-keys)))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
