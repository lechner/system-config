(use-modules (gnu)
             (gnu packages)
             (gnu packages admin)
             (gnu packages certs)
             (gnu packages cups)
             (gnu packages disk)
             (gnu packages file-systems)
             (gnu packages kerberos)
             (gnu packages linux)
             (gnu packages mail)
             (gnu packages nfs)
             (gnu services avahi)
             (gnu services desktop)
             (gnu services mail)
             (gnu services linux)
             (gnu services networking)
             (gnu services pam)
             (gnu services shepherd)
             (gnu system nss)
             (nongnu packages linux)
             (nongnu system linux-initrd))

 (define system-log-service-type
   (shepherd-service-type
    'shepherd-system-log
    (const (shepherd-service
            (documentation "Shepherd's built-in system log (syslogd).")
            (provision '(system-log syslogd))
            (modules '((shepherd service system-log)))
            (free-form #~(system-log-service))))
    #t
    (description
     "Shepherd's built-in system log (syslogd).")))

(define my-pamda-file
  (scheme-file
   "my-pamda.scm"
   #~(begin
       (use-modules ((pam) #:prefix guile-pam:)
                    (pam stack))
       (lambda (action handle flags options)
         ;; for gocryptfs
         (if (eq? 'pam_sm_open_session action)
             (setrlimit 'nofile 100000 100000))
         (stack action handle flags
                (list
                 (let* ((username (guile-pam:get-username handle))
                        (hostname (gethostname))
                        (file (string-append "/acct/"
                                             username
                                             "/"
                                             hostname
                                             "/away/pam.scm")))
                   (gate required (load #$(file-append guile-pam
                                                       "/share/modules/user-session-with-piped-secret.scm"))
                         #:options (list file)
                         #:only-services '("login"
                                           "greetd"
                                           "su"
                                           "slim"
                                           "gdm-password"
                                           "sddm")))))))))

(operating-system
  (host-name "lechner-laptop")
  (timezone "America/Los_Angeles")
  (locale "en_US.utf8")

  (kernel linux)
  (kernel-arguments (cons "consoleblank=60"
                          %default-kernel-arguments))
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  (keyboard-layout (keyboard-layout "us" "altgr-intl"))

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets (list "/efi"))
               (keyboard-layout keyboard-layout)))

  (mapped-devices
   (list (mapped-device
          (source "travelvg")
          (targets (list "travelvg-guix"))
          (type lvm-device-mapping))))

  (file-systems (append (list
                         (file-system
                           (device (file-system-label "GUIX"))
                           (mount-point "/")
                           (type "ext4")
		           (dependencies mapped-devices))

                         (file-system
                           (device "/dev/mapper/travelvg-acct")
                           (mount-point "/acct")
                           (type "ext4")
		           (dependencies mapped-devices))

                         (file-system
                           (device (file-system-label "EFI_624Y6DK"))
                           (mount-point "/efi")
                           (type "vfat")
                           (options "dmask=027,fmask=137")))

                        %base-file-systems))

  (swap-devices
   (list
    (swap-space (target (file-system-label "swap"))
	        (dependencies mapped-devices))))

  (users (append
          (load "../../users.scm")
          %base-user-accounts))

  (groups (append
           (load "../../groups.scm")
           %base-groups))

  (packages (append (list

                     cups
                     efibootmgr
                     gocryptfs
                     gptfdisk
                     heimdal
                     lvm2
                     mdadm
                     nfs-utils
                     smartmontools)

                    %base-packages))

  (services (append
	     (list

              (service system-log-service-type)

              (simple-service 'shepherd-log-rotation
                              shepherd-root-service-type
                              (list (shepherd-service
                                     (provision '(log-rotation))
                                     (modules '((shepherd service log-rotation)))
                                     (free-form #~(log-rotation-service)))))

              (service guile-pam-module-service-type
                       (guile-pam-module-configuration
                        (rules "required")
                        (module my-pamda-file)
                        (services '("login"
                                    "greetd"
                                    "su"
                                    "slim"
                                    "gdm-password"
                                    "sddm"))))

              (service avahi-service-type) ;for guix publish
              (service ntp-service-type)

              (service cachefilesd-service-type
                       (cachefilesd-configuration
                        (cache-directory "/var/cache/fscache")))

              (load "../../service/openssh.scm")
              (load "../../service/krb5-association.scm")
              (load "../../service/cups.scm")
              (load "../../service/udev-rules-net-name-mac.scm")

	      (service opensmtpd-service-type
		       (opensmtpd-configuration
                        (shepherd-requirement '(networking))
		        (config-file
			 (plain-file "opensmtpd-configuration"
                                     "
table secrets file:/etc/mail/secrets
listen on localhost
action \"transmit\" relay host smtp+tls://lechner-laptop@submission.us-core.com:587 auth <secrets>
match from any for any action \"transmit\"
"))))

	      (load "../../service/guix-publish.scm"))

             (load "../../services/greeter.scm")
             (load "../../services/desktop.scm")

             (modify-services
	         %base-services

               (delete agetty-service-type)
               (delete mingetty-service-type)

               (delete syslog-service-type)

	       (guix-service-type
	        config => (guix-configuration

			   (inherit config)

                           (extra-options (list "--gc-keep-derivations=yes" "--gc-keep-outputs=yes"))
                           (discover? #t)
			   (substitute-urls
			    (append (list "https://cuirass.genenetwork.org"
                                          "https://substitutes.nonguix.org")
				    %default-substitute-urls))

			   (authorized-keys
			    (append (list
				     (plain-file "lechner-desktop.pub"
					         "(public-key (ecc (curve Ed25519) (q #219656830F8DB4B7D45358F6AB9DE5FCD2877195D731A3B283A575983443C3DC#)))")
                                     (plain-file "genenetwork.pub"
                                                 "(public-key (ecc (curve Ed25519) (q #9578AD6CDB23BA51F9C4185D5D5A32A7EEB47ACDD55F1CCB8CEE4E0570FBF961#)))")
				     (plain-file "non-guix.pub"
					         "(public-key (ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))

				    %default-authorized-guix-keys)))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
