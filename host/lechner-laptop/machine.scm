(list (machine
       (operating-system (load "operating-system.scm"))
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "lechner-laptop.local")
                       (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKNzagiSVuc1Qrgc4AaaN9UJ0TTrKiggiAhmELYleSRH root@lechner-laptop")
                       (system "x86_64-linux")
                       (allow-downgrades? #t)
                       (identity "/home/lechner/.ssh/id_ed25519")))))
